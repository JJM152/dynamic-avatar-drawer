import {
	clone,
	splitCurve,
	interpolateCurve,
	adjust,
	getPointOnCurve,
	/*
	simpleQuadratic,
	drawPoints,
	extractPoint,
	 */
} from "drawpoint";

/**
	takes the length in y axis and compare it with y positions of array of points
	getLimbPoints(
		the highest reference point (aka 0%),
		the lowest reference point (aka 100%),
		percentage of covered area (0-1),
		point1 (the highest one),
		point2,
		etc.	
	);
	returns array of points in the covered area
	used for sleeves and pants with letiable length
*/
export function getLimbPoints(){
	function point_between(first,second,totalLength){
		let distance = first.y-second.y; //distance between two points 
		let percent =  Math.abs((totalLength-first.y)/distance);//how many percent of distance between two points are covered
		let sp = splitCurve(percent, first, second);	
		let temp = sp.left.p2;
		return temp;
	}

	let input_array = [];
	let output_array = [];
	
	const highest_point = arguments[0];
	const lowest_point = arguments[1];
	const coverage = arguments[2];
		
	const totalLength = highest_point.y-((highest_point.y-lowest_point.y)*coverage);
	//y position of the last point (position of the first - coverage )

	for (let ii = 3; ii < arguments.length; ii++) {
		if(arguments[ii])input_array.push(clone(arguments[ii])); 
	}
	
	//no valid points - returns the first one
	if(totalLength>input_array[0].y){ 
		output_array[0] = input_array[0];
	//all points valid - returns all
	}else if(totalLength<input_array[input_array.length-1].y){ 			
		output_array = input_array;
	//finds the last valid point
	}else{ 
		for(let ii=1;ii<input_array.length;ii++){
			if(totalLength==input_array[ii].y){ //special lucky case
				output_array = input_array.splice(0,(ii+1));
				break;	
			}else if(totalLength>input_array[ii].y){
				let temp = point_between(input_array[ii-1],input_array[ii],totalLength);
				output_array = input_array.splice(0,ii);
				
				output_array[output_array.length] = temp;
				break;
			}
		}
	}
	return output_array;
}
	
/**
	similar to "getLimbPoints" but needs only the lowest point
	getLimbPointsAbovePoint(
		the lowest reference point
		revert (true/false) - returns reversed array (eg for inner points which needs to be drawn in order from the lowest to the highest)
		point1 (the highest one),
		point2,
		etc.	
	);
	used  when you have the lowestmost point on the one side of limb and need to find all the point above it on the other side
*/
export function getLimbPointsAbovePoint(){
	function point_between(first,second,totalLength){
		let temp = interpolateCurve(first,second,{x:null,y:equalizer_point.y});
		let sp = splitCurve(temp[0].t,first,second);	
		return {
			bottom:sp.left.p2,
			second:sp.right.p2
		}
	}

	let input_array = [];
	let output_array = [];
	
	let equalizer_point = arguments[0];
	let totalLength = equalizer_point.y;
	let revert = arguments[1];
	
	for (let ii = 2; ii < arguments.length; ii++) {
		if(arguments[ii])input_array.push(clone(arguments[ii]));
	}
	
	if(totalLength>input_array[0].y){
		output_array[0] = input_array[0];
	}else if(totalLength<input_array[input_array.length-1].y){
		output_array = input_array;
	}else{ 
		for(let ii=1;ii<input_array.length;ii++){
			if(totalLength==input_array[ii].y){
				output_array = input_array.splice(0,(ii+1));
				break;	
			}else if(totalLength>input_array[ii].y){
				let bottom;
				if(revert){
					let temp = point_between(input_array[ii],input_array[ii-1],totalLength);
					bottom = temp.bottom;
					input_array[ii-1] = temp.second;
				}else{
					let temp = point_between(input_array[ii-1],input_array[ii],totalLength);
					bottom = temp.bottom;
				};
				output_array = input_array.splice(0,ii);
				output_array.push(bottom);
				break;
			}
		}
	}
	if(revert)output_array.reverse();
	return output_array;
}
	

/**
	the same as getLimbPoints but returns points that are outside the covered area
		getLimbPointsNegative(
		the highest reference point (aka 0%),
		the lowest reference point (aka 100%),
		percentage of covered area (0-1),
		point1 (the highest one),
		point2,
		etc.	
	);
	used for socks
*/
export function getLimbPointsNegative(){
	function point_between(first,second,totalLength){
		let distance = first.y-second.y; //distance between two points 
		let percent =  Math.abs((totalLength-first.y)/distance);//how many percent of distance between two points are covered
		let sp = splitCurve(percent, first, second);	
		return {
			top:sp.left.p2,
			second:sp.right.p2
		}
	}
	
	let input_array = [];
	let output_array = [];
	
	const highest_point = arguments[0];
	const lowest_point = arguments[1];
	const coverage = arguments[2];
		
	const totalLength = highest_point.y-((highest_point.y-lowest_point.y)*coverage);
	//y position of the last point (position of the first - coverage )

	for (let ii = 3; ii < arguments.length; ii++) {
		if(arguments[ii])input_array.push(clone(arguments[ii]));
	}

	//all points valid - return all
	if(totalLength>input_array[0].y){ 
		output_array = input_array;
	//no valid points - returns the last one
	}else if(totalLength<input_array[input_array.length-1].y){ 		
		output_array[0] = input_array[input_array.length-1];
	//finds the last valid point
	}else{ 
		for(let ii=input_array.length-2;ii>=0;ii--){
			if(totalLength<input_array[ii].y){
				let temp = point_between(input_array[ii],input_array[ii+1],totalLength);
				let top = temp.top
				input_array[ii+1] = temp.second;
				
				input_array.splice(0,ii+1);
				output_array = input_array;
				
				output_array.unshift(top);
				break;
			}
		}
	}
	return output_array;
}
	

	
/**
	similar to "getLimbPointsAbovePoint" but needs only the highest point
	getLimbPointsBellowPoint(
		the highest reference point
		revert (true/false) - returns reversed array (eg for inner points which needs to be drawn in order from the lowest to the highest)
		point1 (the highest one), 
		point2,
		etc.	
	);
	used  when you have the highest point on the one side of limb and need find all the point bellow it on the other side
*/
export function getLimbPointsBellowPoint(){	
	function point_between(first,second,totalLength){
		let temp = interpolateCurve(first,second,{x:null,y:equalizer_point.y});
		let sp = splitCurve(temp[0].t,first,second);	
		temp = sp.left.p2;
		temp.cp1 = sp.left.cp1;
		temp.cp2 = sp.left.cp2;
		return temp;
	}
	
	let input_array = [];
	let output_array = [];
	
	let equalizer_point = arguments[0];
	let totalLength = equalizer_point.y;
	let revert = arguments[1];
	
	for (let ii = 2; ii < arguments.length; ii++) {
		if(arguments[ii])input_array.push(clone(arguments[ii]));
	}
			
	if(totalLength>input_array[0].y){
		output_array = input_array; 
	}else if(totalLength<input_array[input_array.length-1].y){
		output_array[0] = input_array[input_array.length-1];
	}else{ 
		for(let ii=input_array.length-2;ii>=0;ii--){
			if(totalLength<input_array[ii].y){
				
				let temp;
				if(revert){
					temp = point_between(input_array[ii+1],input_array[ii],totalLength);
				}else{
					temp = point_between(input_array[ii],input_array[ii+1],totalLength);
					//UNTESTED! - this case isn't currently used anywhere
				};
				
				input_array.splice(0,ii+1);
				output_array = input_array;

				output_array.unshift(temp);
				break;
			}
		}
	}
	if(revert)output_array.reverse();
	return output_array;
}
	
	
/**
	findBetween(first value,second value, percent)
	returns value with percent distance between first and second value
*/
export function findBetween(higher,lower,percent){
	if(typeof higher == "object")console.log("ERROR! The first variable inputed in findBetween() is not a primitive value (ie is object)! ");
	if(typeof lower == "object")console.log("ERROR! The first variable inputed in findBetween() is not a primitive value (ie is object)! ");
	
	if(isNaN(percent))percent=0.5;//not a number (or undefined)
	let temp; 
	if(lower>higher){temp=higher; higher=lower; lower=temp}
	temp = (Math.abs(higher-lower)*percent)+lower;
	return temp;
}
		
		
/**
	gradually straightens curve
	(by moving both control points closer to the centre of the curve)
*/	

export function straightenCurve(previous_point,point,percent){
	let equilibrium={
		x: findBetween(previous_point.x,point.x,0.5),
		y: findBetween(previous_point.y,point.y,0.5)
	};
		
	if(point.cp1){
		point.cp1.x = findBetween(point.cp1.x,equilibrium.x,percent);
		point.cp1.y = findBetween(point.cp1.y,equilibrium.y,percent);
	}
	
	if(point.cp2){
		point.cp2.x = findBetween(point.cp2.x,equilibrium.x,percent);
		point.cp2.y = findBetween(point.cp2.y,equilibrium.y,percent);
	}
}
		
		
/**
	finds point which is intersection of lines AB and CD
*/	
	
export function lineLineIntersection(A,B,C,D){ 
	//https://www.geeksforgeeks.org/program-for-point-of-intersection-of-two-lines/
	
	// Line AB represented as a1x + b1y = c1 
	let a1 = B.y - A.y; 
	let b1 = A.x - B.x; 
	let c1 = a1*(A.x) + b1*(A.y); 
   
	// Line CD represented as a2x + b2y = c2 
	let a2 = D.y - C.y; 
	let b2 = C.x - D.x; 
	let c2 = a2*(C.x)+ b2*(C.y); 
   
	let determinant = a1*b2 - a2*b1; 
   
	if (determinant == 0){
		console.log("Err - lineLineIntersection - they are paralel");
		// The lines are parallel. This is simplified 
		return undefined;
	}else{ 
		let x = (b2*c1 - b1*c2)/determinant; 
		let y = (a1*c2 - a2*c1)/determinant; 
		return {x:x, y:y}; 
	} 
}


/**
	finds point which is intersection of bezier curve and line AB 
	lineCubicIntersection(startpoint,endpoint,A,B){
*/	

export function lineCubicIntersection(start,end,lineA,lineB){
	//https://www.particleincell.com/2013/cubic-line-intersection/
		
	//asap - it is in curve.js made it importable
	function solveCubicEquation(a, b, c) {
		const a3 = a / 3;
		const p = (3 * b - a * a) / 3;
		const p3 = p / 3;
		const q = (2 * a * a * a - 9 * a * b + 27 * c) / 27;
		const q2 = q / 2;
		const discriminant = roundToDec(q2 * q2 + p3 * p3 * p3, 8);

		if (discriminant > 0) {
			const sqrtDiscriminant = Math.sqrt(discriminant);
			const u = cubeRoot(-q2 + sqrtDiscriminant);
			const v = cubeRoot(q2 + sqrtDiscriminant);
			const x1 = u - v - a3;
			// ignore other imaginary roots
			return [x1];
		}

		// all roots real (3 in total, 1 single and 1 double)
		if (discriminant === 0) {
			// v = -u
			const u = cubeRoot(-q2);
			// t = u - v, x = t - a/3 = u - v - a/3 = 2u - a/3
			const x1 = 2 * u - a3;
			// conjugate roots produce 1 double root
			const x2 = -u - a3;
			return [x1, x2];
		}
		const r = Math.sqrt(-p3 * p3 * p3);
		let cosphi = -q2 / r;
		if (cosphi < -1) {
			cosphi = -1;
		} else if (cosphi > 1) {
			cosphi = 1;
		}
		const phi = Math.acos(cosphi);
		const commonPrefix = 2 * cubeRoot(r);
		const x1 = commonPrefix * Math.cos(phi / 3) - a3;
		const x2 = commonPrefix * Math.cos((phi + 2 * Math.PI) / 3) - a3;
		const x3 = commonPrefix * Math.cos((phi + 4 * Math.PI) / 3) - a3;
		return [x1, x2, x3];
	}
	
	function cubeRoot(v) {
        if (v < 0) {
            return -Math.pow(-v, 1 / 3);
        } else {
            return Math.pow(v, 1 / 3);
        }
    }
	
	function roundToDec(num, numDecimals) {
        return parseFloat(num.toFixed(numDecimals));
    }
	
	function roundToDecimal(num, dec) {
        return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    }

	let A=lineB.y-lineA.y;	    //A=y2-y1
	let B=lineA.x-lineB.x;	    //B=x1-x2
	let C= lineA.x*(lineA.y-lineB.y)+lineA.y*(lineB.x+lineA.x);
		  
	function bezierCoeffs(P0,P1,P2,P3){
		let Z = Array();
		Z[0] = -P0 + 3*P1 + -3*P2 + P3; 
		Z[1] = 3*P0 - 6*P1 + 3*P2;
		Z[2] = -3*P0 + 3*P1;
		Z[3] = P0;
		return Z;
	}

	let bx = bezierCoeffs(start.x, end.cp1.x, end.cp2.x, end.x);
	let by = bezierCoeffs(start.y, end.cp1.y, end.cp2.y, end.y);
 
	let P = Array();
	P[0] = A*bx[0]+B*by[0];		/*t^3*/
	P[1] = ( A*bx[1]+B*by[1] )/P[0] ;	/*t^2*/
	P[2] = ( A*bx[2]+B*by[2] )/P[0];	/*t*/
	P[3] = ( A*bx[3]+B*by[3] + C )/P[0];	/*1*/
 
	let roots=solveCubicEquation(P[1],P[2],P[3]);


	let t;
	let root = void 0;
	for (let i = 0; i < roots.length; i++) {
		root = roundToDecimal(roots[i], 15);
		if (root >= 0 && root <= 1) {
		  t=root;
		}
	}
	
	let X=bx[0]*t*t*t+bx[1]*t*t+bx[2]*t+bx[3];
	let Y=by[0]*t*t*t+by[1]*t*t+by[2]*t+by[3];  
	
	//ASAP - ASUMES THERE IS ONLY ONE VALID POINT
	return {x:X,y:Y};
}




/**
	finds if point C is located on line defined by points A and B
	(returns 0 if it is)
*/
export function pointLineIntersection(A,B,C){
	function lineEquation(A,B){
		let m=(
			(B.y-A.y)/(B.x-A.x)
		);

		let b=(
			A.y-(m*A.x)
		);	

		//y = mx + b
		return{
			m:m,
			b:b,
		};
	}
	let temp = lineEquation(A,B);
	//y - mx - b = 0;
	return (C.y-(temp.m*C.x)-temp.b);
}
		


/**
	returns points for drawing lacing between two curves
	getLacingPoints(
		startpoint of the inner curve
		endpoint of the inner curve
		startpoint of outer
		endpoint of outer
		number of crossings
		adjustment (to not have lace hole straight on the curve)
	);
*/
export function getLacingPoints(innerStart,innerEnd,outerStart,outerEnd,count,adjustment=0){
	count++;
	let inner = [];
	let outer = [];
	
	let step = 1/count;
	
	for(let ii = 0; (ii*step)<=1; ii++){
		if(ii%2!=0){
			inner[ii]= adjust(getPointOnCurve(step*ii,innerStart,innerEnd),adjustment,0)
			outer[ii]= adjust(getPointOnCurve(step*ii,outerStart,outerEnd),-adjustment,0)
		}else{
			inner[ii]= adjust(getPointOnCurve(step*ii,outerStart,outerEnd),-adjustment,0)
			outer[ii]= adjust(getPointOnCurve(step*ii,innerStart,innerEnd),adjustment,0)
		}
	}
	
	return {inner: inner,outer:outer};
}


/**
	first finds a point T on the line between points "A" and "B" with distance from "A" "percent"*AB
	then returns point C perpendicular to line AB with distance from the point T "distance"
	
		C
		|
	A---T----B
	   
	perpendicular_point(
		A - fist point {x,y}
		B - second point {x,y}
		percent - percentage of distance AB (values 0-1)
		distance - perpendicular distance from a point on line AB and the resulting point C
	)
	returns C
*/
export function perpendicularPoint(A,B,percent,distance){
	let H = {x:findBetween(A.x,B.x,percent),y:findBetween(A.y,B.y,percent) }; //half point
	let base_angle = Math.atan2(B.y-A.y,B.x-A.x);  //atan2(y,x) - first y!!!
	let bbb = Math.sqrt(  
		Math.pow(H.y-A.y,2)+Math.pow(H.x-A.x,2)
	);
	let aaa = distance;
	let ccc = Math.sqrt(
		Math.pow(aaa,2)+Math.pow(bbb,2)
	);
	
	let alfa = Math.atan(aaa/bbb);

	return {
			x:A.x+( ccc*Math.cos(alfa+base_angle) ),
			y:A.y+( ccc*Math.sin(alfa+base_angle) )
	};
}