import {FacePart} from "./face_part";
import {adjustColor} from "../util/utility";
import {
    breakPoint,
    endPoint
} from "drawpoint";

class Mouth extends FacePart {
    constructor(...data) {
        super({
            loc       : "mouth",
            reflect   : true,
            aboveParts: ["parts head"],
        }, ...data);
    }
}


export class MouthHuman extends Mouth {
    constructor(...data) {
        super(...data);
    }

    fill(ignore, ex) {
        return adjustColor(ex.baseLipColor,
            {
                s: -20,
                l: -10
            });
    }

    clipFill() {
    }

    calcDrawPoints(ex) {
        return [
            breakPoint,
            ex.lips.top,
            ex.lips.out,
            ex.lips.bot,
            ex.lips.top,
            endPoint,
        ];
    }
}

