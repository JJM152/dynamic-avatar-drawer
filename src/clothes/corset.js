import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
//import {connectEndPoints} from "../draw/draw";
import {Layer} from "../util/canvas";
import {
	breakPoint,
    extractPoint,
	drawPoints, 
	splitCurve,
	simpleQuadratic,
	clone,
	adjust,
} from "drawpoint";

import {
	getLimbPointsBellowPoint,
	//getLimbPoints,
	//findBetween,
	getLacingPoints,
} from "../util/auxiliary";

import {Top} from "./tops";
import {calcBra} from "./underwear";

export class CorsetBreastPart extends ClothingPart {
	constructor(...data) {
        super({
            layer     : Layer.GENITALS,
            loc       : "chest",
            reflect   : true,
            aboveParts: ["parts chest", "decorativeParts chest"],
        }, ...data);
    }
	
	renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		if (ex.hasOwnProperty("breast") === false) {
			ctx.save();
			ctx.lineWidth = 4;
			ctx.strokeStyle = ctx.fillStyle;
				 
			ctx.beginPath();
			drawPoints(ctx, breakPoint, ex.chest.nipples);
			ctx.stroke();
			ctx.restore();
			return;
		}
		
		const bra = calcBra(ex);
      
		bra.out.cp1 = simpleQuadratic(bra.top, bra.out, 0.4, 1);
		bra.top.cp1 = simpleQuadratic(ex.breast.cleavage, bra.top, 0.6, 2);

		ctx.beginPath();
		drawPoints(ctx,
			bra.top,
			bra.out,
			bra.tip,
			ex.breast.bot,
			ex.breast.in,
			ex.breast.cleavage,
			bra.top
		);
		ctx.fill();
		
		drawPoints(ctx,
			ex.breast.cleavage,
			bra.top,
			bra.out,
			bra.tip,
			ex.breast.bot
		);
		ctx.stroke();
		
	
	}	
}


function calcCorset(ex){
	let top;
	let topMid;
	
	if(!ex.breast){
		top = clone(ex.armpit);
		topMid = {
			x:-0.1,
			y:top.y-4
		};
		top.cp1 = {
			x: top.x * 0.5 + topMid.x * 0.5,
			y: topMid.y
		};
	}else{
		let temp = getLimbPointsBellowPoint(ex.breast.cleavage,false,ex.armpit,ex.waist);
		top = temp[0];
		topMid = {
			x:-0.1,
			y:ex.breast.cleavage.y
		};
	}
	
	
	let temp = splitCurve(this.botCoverage,ex.waist,ex.hip);
	let bot =  temp.left.p2;
		
	let botMid = {
		x:0,
		y:bot.y-7
	};
		botMid.cp2 = {
			x: botMid.x * 0.5 + bot.x * 0.5,
			y: botMid.y
		};
			
		botMid.cp1 = {
			x: botMid.cp2.x+12,
			y: botMid.cp2.y+9,
		};
			
	return{
		topMid,
		top,
		bot,
		botMid
	};
}

export class CorsetPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "torso",
            reflect            : true,
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		const {topMid,top,bot,botMid} = calcCorset.call(this, ex);

		ctx.beginPath();
		drawPoints(ctx,	
			topMid,
			top,
			ex.waist,
			bot,
			extractPoint(bot),
			botMid
			//topMid
		);
		ctx.fill();	
		/*
		ctx.beginPath();
		drawPoints(ctx, 
			topMid,
			top,
			ex.waist,
			bot,
			extractPoint(bot),
			botMid 
		);
		*/
		ctx.stroke();
		
		
		//LACING
		if(this.lacing){
			let inTop = adjust(topMid,-3,-1);
			let inBot = adjust(botMid,-3,1);
			let outTop = adjust(topMid,3,-1);
			let outBot = adjust(botMid,3,1);
			let lacing = getLacingPoints(inBot,inTop,outBot,outTop,this.knots);

			ctx.strokeStyle = this.highlight; 
			ctx.beginPath();
			drawPoints(ctx, ...lacing.inner, breakPoint,...lacing.outer);
			ctx.stroke();	
		}
		
    }
}

export class HalfCorsetPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "torso",
            reflect            : true,
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		let temp = splitCurve(1-this.topCoverage,ex.armpit,ex.waist);
		let top =  extractPoint(temp.left.p2); 
			temp = splitCurve(this.botCoverage,ex.waist,ex.hip);
		let bot =  temp.left.p2;
			
		let botMid = {
			x:0,
			y:bot.y-7
		};
			botMid.cp2 = {
				x: botMid.x * 0.5 + bot.x * 0.5,
				y: botMid.y
			};
				
			botMid.cp1 = {
				x: botMid.cp2.x+12,
				y: botMid.cp2.y+9,
			};
			
		let topMid = {
				x:0,
				y:top.y-5
			};
			top.cp1 = {
				x: top.x * 0.5 + topMid.x * 0.5,
				y: topMid.y
			};			
	
		ctx.beginPath();
		drawPoints(ctx,	
			topMid,
			top,
			ex.waist,
			bot,
			botMid, 
			topMid
		);
		ctx.fill();	
		
		ctx.beginPath();
		drawPoints(ctx, 
			topMid,
			top,
			ex.waist,
			bot,
			botMid 
		);
		ctx.stroke();
		
		//lacing
		if(this.lacing){
			let inTop = adjust(topMid,-3,-1);
			let inBot = adjust(botMid,-3,1);
			let outTop = adjust(topMid,3,-1);
			let outBot = adjust(botMid,3,1);
			let lacing = getLacingPoints(inBot,inTop,outBot,outTop,this.knots);

			ctx.strokeStyle = this.highlight; 
			ctx.beginPath();
			drawPoints(ctx, ...lacing.inner, breakPoint, ...lacing.outer);
			ctx.stroke();	
		}
		
    }
}

/* */

export class Corset extends Top { //dress?
    constructor(...data) {
        super({
			clothingLayer  : Clothes.Layer.OUTER, //asap!!
			botCoverage: 1,
			thickness: 1,
			lacing:true,
			knots: 6,
			highlight: "hsla(0, 0%, 52%, 1)",
        }, ...data);
    }

    stroke() {
        return "#000";
    }

    fill() {
        return "hsl(0,10%,20%)";
    }

    get partPrototypes() {
        return [
			{ 
                side: null,  
                Part: CorsetBreastPart
            },{ 
                side: null,  
                Part: CorsetPart
            },

        ];
    }
}	

export class HalfCorset extends Top { //dress?
    constructor(...data) {
        super({
			clothingLayer  : Clothes.Layer.OUTER, //asap!!
			topCoverage: 0.6,
			botCoverage: 1,
			thickness: 1,
			knots: 6,
			lacing:true,
			highlight: "hsla(0, 0%, 52%, 1)",
        }, ...data);
    }

    stroke() {
        return "#000";
    }

    fill() {
        return "hsl(0,10%,20%)";
    }

    get partPrototypes() {
        return [{ 
                side: null,  
                Part: HalfCorsetPart
            },

        ];
    }
}