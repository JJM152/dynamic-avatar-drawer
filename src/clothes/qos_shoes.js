import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {Layer} from "../util/canvas";
import {ShadingPart} from "../draw/shading_part";
import { adjustPoint, simpleQuadratic, drawPoints, extractPoint, clone, splitCurve} from "drawpoint";
import { getLimbPointsNegative, getLimbPointsBellowPoint, getLimbPointsAbovePoint, findBetween} from "../util/auxiliary";


export class LeftBaseBootShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "left feet",
            layer: Layer.FRONT,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const toeBox = ex.toe.toebox;

        const sp = splitCurve(0.5, ex.toe.out, ex.toe.in);
        const inner = sp.right.p2;
        const bot = sp.left.p2;
        bot.cp1 = simpleQuadratic(toeBox, bot, 0.5, 1);

        const ankleInBot = clone(ex.ankle.inbot);
        toeBox.cp1 = {
            x: ankleInBot.x - 1,
            y: ankleInBot.y - 5
        };
        toeBox.cp2 = {
            x: toeBox.x - 3,
            y: toeBox.y
        };

        return [toeBox, bot, inner, ankleInBot, toeBox];

    }
}


export class RightBaseBootShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "right feet",
            layer: Layer.FRONT,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const toeBox = ex.toe.toebox;

        const sp = splitCurve(0.8, ex.toe.out, ex.toe.in);
        const bot = sp.left.p2;
        bot.cp1 = simpleQuadratic(toeBox, bot, 0.5, -1);

        const ankleOutBot = extractPoint(ex.ankle.outbot);
        ankleOutBot.cp1 = simpleQuadratic(bot, ankleOutBot, 0.5, -10);

        toeBox.cp1 = simpleQuadratic(ankleOutBot, toeBox, 0.7, 3);

        return [ankleOutBot, toeBox, bot, ankleOutBot];

    }
}

export class BootBasePart extends ClothingPart {
    constructor(...data) {
        super({
            layer       : Layer.FRONT,
            loc         : "feet",
            aboveParts: ["parts feet", "parts leg"],
            belowParts: ["shadingParts feet"],
            shadingParts: [LeftBaseBootShading, RightBaseBootShading]
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        // clamp args
        let legCoverage = 1 - Math.min(this.legCoverage, 1); //reverse this since we draw from the top

        ex.toe.toebox = {
            x: ex.toe.center.x,
            y: ex.toe.center.y + this.toeHeight
        };

        // Add points for quads
        let  addPointsMid=[];
        if(typeof ex.quads !== "undefined"){
            addPointsMid[0] =  clone(ex.quads.top);
            addPointsMid[0].x = ex.thigh.out.x;
            addPointsMid[1] = clone(ex.quads.out);
        }

        //Adjust some points
        const ankleOut = ex.ankle.out;
        ankleOut.x += 1;
        const ankleIn = ex.ankle.in;
        ankleIn.x -= 1;
        const out = ex.ankle.outbot;
        const outBot = ex.toe.out;
        const inBot = ex.toe.in;
        inBot.x -= 2;

        // Get the points beween ex.hip and ex.ankle.out relative to this.legCoverage
        let  outerPoints = getLimbPointsNegative(ex.hip, ex.ankle.out, legCoverage,
                ex.hip, ex.thigh.out, ...addPointsMid, ex.knee.out, ex.calf.out, ankleOut);

        // Grab all the inner points below the highest outerpoint
        let  innerPoints = getLimbPointsBellowPoint(outerPoints[0], true,
                ex.groin, ex.thigh.top, ex.thigh.in, ex.knee.intop, ex.knee.in, ex.calf.in, ankleIn);
		
        outerPoints[0] = extractPoint( outerPoints[0]); 

        //If the adjusted highest outer point is <= the top of the thigh, add a curve point to
        //the highest outer point
        if(outerPoints[0].y-6 <= ex.thigh.top.y){
            outerPoints[0].cp1 = {
                x: findBetween( innerPoints[innerPoints.length-1].x, outerPoints[0].x, 0.5),
                y:outerPoints[0].y-6
            };
        
        } else {
            // I have to look at this more, but it appears to find a point on the y axis between
            // the hip and the thigh.out and then adds a curve point to the newly found point
            innerPoints.splice(innerPoints.length-1, 1);
            let temp = interpolateCurve( ex.hip, ex.thigh.out, {x:null,y:ex.thigh.top.y});
            outerPoints[0] = extractPoint(temp[0]);
            outerPoints[1] = extractPoint(outerPoints[1]);//lasso loop above outerPoints[0]; 
            outerPoints[0].cp1 = {
                x:findBetween(innerPoints[innerPoints.length-1].x,outerPoints[0].x,0.5),
                y:outerPoints[0].y-6
            };		
        }

        Clothes.simpleStrokeFill(ctx, ex, this);

        ctx.beginPath();
        drawPoints(ctx,
            ...outerPoints,
            out, outBot, inBot, 
            ...innerPoints,
            outerPoints[0] // close it off
        );
        ctx.fill();
        ctx.stroke();

    }
}

export class QueenShoes extends Clothing {
    constructor(...data) {
		super({
			clothingLayer  : Clothes.Layer.MID,
			thickness: 0.6,
		}, ...data);
    }
	
    stroke() {
        return "#000";
    }

    fill() {
        return "hsl(0,10%,20%)";
    }
}

export class QueenBoots extends QueenShoes {
    constructor(...data) {
        super({
            clothingLayer   : Clothes.Layer.MID,
            legCoverage : 0.5,
            shoeHeight      : 0,
            tongueDeflection: 2,
            toeHeight       : 0,
            Mods            : {
                feetLength: -10,
                feetWidth : -2
            }
        }, ...data);
    }



    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: BootBasePart
            },
            {
                side: Part.RIGHT,
                Part: BootBasePart
            },
        ];
    }
}

