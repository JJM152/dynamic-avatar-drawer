import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {connectEndPoints} from "../draw/draw";
import {Layer} from "../util/canvas";
import {
    extractPoint,
	drawPoints, 
	splitCurve,
	//clone,
	//adjust,
} from "drawpoint";

import {
	getLimbPointsNegative,
	getLimbPointsBellowPoint,
	//getLacingPoints,
} from "../util/auxiliary";

export class GlovePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
            armCoverage: 0.5,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		let outerArmPoints = getLimbPointsNegative(ex.collarbone,ex.hand.palm,this.armCoverage,ex.collarbone,ex.deltoids,ex.shoulder,ex.elbow.out,ex.wrist.out,ex.hand.knuckle,ex.hand.tip);
		let innerArmPoints = getLimbPointsBellowPoint({y:outerArmPoints[0].y-3,x:outerArmPoints[0].x},true,ex.armpit,ex.elbow.in,ex.wrist.in,ex.thumb.out,ex.thumb.tip);

		if(this.armCoverage>0){
			ctx.beginPath();
			drawPoints(ctx, 
				...outerArmPoints,
				ex.hand.palm,			
				...innerArmPoints,
				extractPoint(outerArmPoints[0])
			);
			ctx.fill();
			ctx.stroke();
		}else{ //for transformations
			//let con_collarbone = adjust(ex.collarbone,-0.5,0);
			//let con_armpit = adjust(ex.armpit,-0.5,0);
				
			ctx.beginPath();
			drawPoints(ctx, 
				...outerArmPoints,
				ex.hand.palm,			
				...innerArmPoints,
				connectEndPoints(innerArmPoints[innerArmPoints.length-1],outerArmPoints[0])
			);
			ctx.fill();
			ctx.stroke();
		}
    }
}


export class FingerlessGlovePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
            armCoverage: 0.5,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		 Clothes.simpleStrokeFill(ctx, ex, this);
		  
		let temp;
		let outerArmPoints = getLimbPointsNegative(ex.collarbone,ex.hand.palm,this.armCoverage,ex.collarbone,ex.deltoids,ex.shoulder,ex.elbow.out,ex.wrist.out,ex.hand.knuckle);
		let innerArmPoints = getLimbPointsBellowPoint({y:outerArmPoints[0].y-3,x:outerArmPoints[0].x},true,ex.armpit,ex.elbow.in,ex.wrist.in,ex.thumb.out);
		
			temp = splitCurve(0.3, ex.hand.knuckle,ex.hand.tip); 
		let outIndex = extractPoint(temp.left.p2);
		
			temp = splitCurve(0.8, ex.hand.tip,ex.hand.palm); 
		let inIndex = extractPoint(temp.left.p2);
		
			temp = splitCurve(0.3,ex.thumb.tip,ex.thumb.out);
		let outThumb = extractPoint(temp.left.p2);
		
		if(this.armCoverage>0){
			ctx.beginPath();
			drawPoints(ctx, 
				...outerArmPoints,
				outIndex,
				inIndex,
				extractPoint(ex.hand.palm),
				outThumb,
				...innerArmPoints,
				extractPoint(outerArmPoints[0])
			);
			ctx.fill();
			ctx.stroke();
		}else{ //for transformations
			//let con_collarbone = adjust(ex.collarbone,-0.5,0);
			//let con_armpit = adjust(ex.armpit,-0.5,0);
				
			ctx.beginPath();
			drawPoints(ctx, 
				...outerArmPoints,
				outIndex,
				inIndex,
				extractPoint(ex.hand.palm),
				outThumb,
				...innerArmPoints,
				connectEndPoints(innerArmPoints[innerArmPoints.length-1],outerArmPoints[0])
			);
			ctx.fill();
			ctx.stroke();
		}		
    }
}


export class BraceletPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
            armCoverage: 0.5,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		let outerArmPoints = getLimbPointsNegative(ex.collarbone,ex.hand.palm,this.armCoverage,ex.collarbone,ex.deltoids,ex.shoulder,ex.elbow.out,ex.wrist.out);
		let innerArmPoints = getLimbPointsBellowPoint({y:outerArmPoints[0].y-3,x:outerArmPoints[0].x},true,ex.armpit,ex.elbow.in,ex.wrist.in);
		
		if(this.armCoverage>0){
			ctx.beginPath();
			drawPoints(ctx, 
				...outerArmPoints,
				extractPoint(ex.wrist.out),
				ex.wrist.in,
				...innerArmPoints,
				extractPoint(outerArmPoints[0])
			);
			ctx.fill();
			ctx.stroke();
		}else{ //for transformations
			//let con_collarbone = adjust(ex.collarbone,-0.5,0);
			//let con_armpit = adjust(ex.armpit,-0.5,0);
				
			ctx.beginPath();
			drawPoints(ctx, 
				...outerArmPoints,
				ex.wrist.out,
				ex.wrist.in,
				...innerArmPoints,
				connectEndPoints(innerArmPoints[innerArmPoints.length-1],outerArmPoints[0])
			);
			ctx.fill();
			ctx.stroke();
		}
		
    }
}

export class Glove extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.BASE,
            thickness    : 0.8,
        }, ...data);
    }
	
	fill(){
        return "hsla(0, 0%, 28%, 1)";
    }
}


export class LongGloves extends Glove {
    constructor(...data){
        super({
			armCoverage:0.5,
		},...data);
    }

	fill(){
        return "hsla(0, 0%, 28%, 1)";
    }
	
    get partPrototypes(){
        return [
           	{
                side: Part.LEFT,
                Part: GlovePart,
            },
            {
                side: Part.RIGHT,
                Part: GlovePart,
            },
        ];
    }
}

export class FingerlessGloves extends Glove {
    constructor(...data){
        super({
			armCoverage:0.9,
		},...data);
    }

	fill(){
        return "hsla(0, 0%, 28%, 1)";
    }
	
    get partPrototypes(){
        return [
           	{
                side: Part.LEFT,
                Part: FingerlessGlovePart,
            },
            {
                side: Part.RIGHT,
                Part: FingerlessGlovePart,
            },	
        ];
    }
}

export class Bracelet extends Glove {
    constructor(...data){
        super({
			armCoverage:0.75,
		},...data);
    }

	fill(){
        return "hsla(0, 0%, 28%, 1)";
    }
	
    get partPrototypes(){
        return [
           	{
                side: Part.LEFT,
                Part: BraceletPart,
            },
            {
                side: Part.RIGHT,
                Part: BraceletPart,
            },	
        ];
    }
}