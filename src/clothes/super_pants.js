import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {Layer} from "../util/canvas";
import {
	drawPoints,
    extractPoint,
    splitCurve,
    clone,
	adjust,
} from "drawpoint";

import {Pants,CoveredButtPart} from "./pants";
import {
	findBetween,
	getLimbPoints,
	getLimbPointsAbovePoint,
} from "../util/auxiliary";


function calcSuperPants(ex, waistCoverage,legCoverage,outerLoose,innerLoose,bustle) {
	const max_skirt_width = 50;

	let out;
	let hip;
	let thighOut;
		
	//TOP
	if(waistCoverage>0){
		let temp = splitCurve((1-waistCoverage),ex.waist,ex.hip);	
		out = extractPoint(temp.left.p2);
		hip = temp.right.p2;
        thighOut = ex.thigh.out;
	}else{
		let temp = splitCurve(Math.abs(waistCoverage),ex.hip,ex.thigh.out);	
		out = extractPoint(temp.left.p2);
		//hip;
		thighOut = temp.right.p2;
	}
	
	const waistCurve = out.y - ex.hip.y;
	let top = {
		x: -0.1,  
		y: ex.pelvis.y + waistCurve * 1.2
	};
	out.cp1 = {
		x: out.x * 0.5 + top.x * 0.5,
		y: top.y
	};
	
	let groin = adjust(ex.groin,-0.1,0);
	if(innerLoose<1){
		groin.cp1 = {
			x: groin.x * 0.5 + ex.thigh.top.x * 0.5,
			y: groin.y
		};
	}
		
		
	let outerPoints = [];
			
	const addPointsStart = []; //ie should be hip included?
		addPointsStart[0] = out;
		if(out.y>ex.hip.y)addPointsStart[1]=hip;
			
	const addPointsMid = []; //ie should be muscles included?
		if(typeof ex.quads !== "undefined"){
			addPointsMid[0] =  clone(ex.quads.top);
			addPointsMid[0].x =  ex.thigh.out.x;
			addPointsMid[1] = clone(ex.quads.out);
		}
				
	if(outerLoose>=1){ //SKIRT
		let totalLegLength = ex.hip.y-( (ex.hip.y-ex.ankle.out.y)*legCoverage );
		if (totalLegLength<ex.thigh.out.y){ //long skirt
			outerPoints = getLimbPoints(ex.hip,ex.ankle.out,legCoverage,...addPointsStart,thighOut);

      let index;
			if(bustle){
				index=outerPoints.length;
				outerPoints[index] = extractPoint(ex.thigh.out);
			}else{
				index=outerPoints.length-1;
				outerPoints[index] = clone(ex.thigh.out);
			}
			outerPoints[index].y = totalLegLength;
			outerPoints[index].x += (outerLoose-1)*(max_skirt_width*legCoverage);
		
		}else{ //short skirt
			outerPoints = getLimbPoints(ex.hip,ex.ankle.out,legCoverage,out,thighOut);
			outerPoints[outerPoints.length-1].y = totalLegLength;
			outerPoints[outerPoints.length-1].x += (outerLoose-1)*(max_skirt_width*legCoverage);
		}
		
	}else if(outerLoose>0){ //LOOSE
		let max;
		if(typeof ex.quads !== "undefined"){
			max = findBetween(ex.quads.out.x,ex.quads.out.cp1.x,0.5);
		}else{ 
			max = ex.thigh.out.x;
		}
		
		outerPoints = getLimbPoints(ex.hip,ex.ankle.out,legCoverage,...addPointsStart,thighOut,...addPointsMid,ex.knee.out,ex.calf.out,ex.ankle.out);
		
		//adjust X but not more than max & smooth control points 
		let skipped_points=addPointsStart.length+1; //hip+thigh;
		if(addPointsMid.length>0)skipped_points++;//+muscles
		
		for(let ii=skipped_points;ii<outerPoints.length;ii++){
			outerPoints[ii].x = findBetween(outerPoints[ii].x,max,outerLoose);
			if(outerPoints[ii].cp1){ outerPoints[ii].cp1.x = findBetween(outerPoints[ii].cp1.x,max,outerLoose) }
      if(outerPoints[ii].cp2){ outerPoints[ii].cp2.x = findBetween(outerPoints[ii].cp2.x,max,outerLoose) }
		}	
	}else{//TIGHT
		outerPoints = getLimbPoints(ex.hip,ex.ankle.out,legCoverage,out,hip,thighOut,...addPointsMid,ex.knee.out,ex.calf.out,ex.ankle.out);
	}		
		
	//INNER
	let innerPoints = [];
		//?? use caluculated groin
	if(innerLoose>=1){
		innerPoints = getLimbPointsAbovePoint(outerPoints[outerPoints.length-1],true,top,ex.thigh.in,ex.knee.intop,ex.knee.in,ex.calf.in,ex.ankle.in);
	}else{
		innerPoints = getLimbPointsAbovePoint(outerPoints[outerPoints.length-1],true,groin,ex.thigh.in,ex.knee.intop,ex.knee.in,ex.calf.in,ex.ankle.in);
	}

	innerPoints[0] = extractPoint(innerPoints[0]);
	
	//ADJUST INNER POINTS to not reach into negative values 
 	if(innerLoose>0){
		for (let ii=0; ii<innerPoints.length; ii++){ 
			innerPoints[ii].x -=  ex.ankle.in.x*innerLoose;    
			if(innerPoints[ii].x<0){innerPoints[ii].x=-0.1}
			
			if(typeof innerPoints[ii].cp1 !== "undefined"){
					innerPoints[ii].cp1.x = innerPoints[ii].cp1.x-(innerPoints[ii].cp1.x*innerLoose);
					if(innerPoints[ii].cp1.x<1){innerPoints[ii].cp1.x=-0.1}
			}
			if(typeof innerPoints[ii].cp2 !== "undefined"){
					innerPoints[ii].cp2.x = innerPoints[ii].cp2.x-(innerPoints[ii].cp2.x*innerLoose);
					if(innerPoints[ii].cp2.x<1){innerPoints[ii].cp2.x=-0.1}
			}
		}
	}
	
	//CURVED BOTTOM
	if(innerLoose<1&&innerPoints[0].y<ex.groin.y){ //pants
		innerPoints[0].cp1 = {
			x : findBetween(outerPoints[outerPoints.length-1].x,innerPoints[0].x,0.5),
			y : innerPoints[0].y+3,
		};	
	}else if(innerLoose>=1){ //skirt
		outerPoints[outerPoints.length-1].y += 2;
		innerPoints[0].y += -2;
		innerPoints[0].cp1 = {
			x: innerPoints[0].x * 0.5 + outerPoints[outerPoints.length-1].x * 0.5,
			y: innerPoints[0].y 
		};
	}	
			

	if(out.y>ex.hip.y&&out.y<ex.hip.y+10){ //there is loose loop visible around belt line and looks like shit
		if(typeof outerPoints[1]!=="undefined")outerPoints[1]=extractPoint(outerPoints[1]);
	}


    return {
        out,
        hip,
        top,
        thighOut,
        groin,
		outerPoints,
		innerPoints,
    };
}




export class SuperPantsPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "leg",
            aboveParts: ["parts leg"],
		//	belowSameLayerParts: ["torso"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        const {out, hip, top, thighOut, groin, outerPoints, innerPoints} = calcSuperPants(ex, this.waistCoverage,this.legCoverage,this.outerLoose,this.innerLoose,this.bustle);

        Clothes.simpleStrokeFill(ctx, ex, this);

		if(this.innerLoose>=1){ //skirt - do not draw the middle line
			ctx.beginPath();
			drawPoints(ctx, top, ...outerPoints, innerPoints[0],top);
			ctx.fill(); 
			
			ctx.beginPath();
			drawPoints(ctx, ...outerPoints, innerPoints[0]);
			ctx.stroke();
		}else{
			ctx.beginPath();
			drawPoints(ctx, top, ...outerPoints,  ...innerPoints, groin, top);
			ctx.fill(); 
			
			ctx.beginPath();
			drawPoints(ctx, ...outerPoints,  ...innerPoints, groin);
			ctx.stroke();
		}
		
		//belt
		ctx.beginPath();
		ctx.lineWidth = 2;
		drawPoints(ctx, top,adjust(out,-0.5,0));
		ctx.stroke();
	}
}

/**
	the same thing as SuperPantsPart, but drawn BELLOW shirts
*/
export class SuperLegginsPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "leg",
            aboveParts: ["parts leg"],
			belowSameLayerParts: ["torso"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        const {out, hip, top, thighOut, groin, outerPoints, innerPoints} = calcSuperPants(ex, this.waistCoverage,this.legCoverage,this.outerLoose,this.innerLoose,this.bustle);

        Clothes.simpleStrokeFill(ctx, ex, this);

		if(this.innerLoose>=1){ //skirt - do not draw the middle line
			ctx.beginPath();
			drawPoints(ctx, top, ...outerPoints, innerPoints[0],top);
			ctx.fill(); 
			
			ctx.beginPath();
			drawPoints(ctx, ...outerPoints, innerPoints[0]);
			ctx.stroke();
					
		}else{
			ctx.beginPath();
			drawPoints(ctx, top, ...outerPoints,  ...innerPoints, groin, top);
			ctx.fill(); 
			
			ctx.beginPath();
			drawPoints(ctx, ...outerPoints,  ...innerPoints, groin);
			ctx.stroke();
		}
		
		//belt
		ctx.beginPath();
		ctx.lineWidth = 2;
		drawPoints(ctx, top,adjust(out,-0.5,0));
		ctx.stroke();
	}
}


export class SuperPants extends Pants {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.MID,
            innerLoose: 0,
			outerLoose: 0,
			legCoverage: 0.9,
			waistCoverage: 0.5,
			opacity: 1,
			thickness: 1,
			bustle:false,
        }, ...data);
    }

	fill() {
        return "hsl(200, 65%, 20%)";
    }
	
	stroke() {
       return "hsl(200, 50%, 10%)";
    }
	
    get partPrototypes() {
        return [
            {
                side: null,
                Part: CoveredButtPart,
            },
            {
                side: Part.LEFT,
                Part: SuperPantsPart
            },
            {
                side: Part.RIGHT,
                Part: SuperPantsPart
            },
        ];
    }
}




export class SuperLeggins extends Pants {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.MID,
            innerLoose: 0,
			outerLoose: 0,
			legCoverage: 0.9,
			waistCoverage: 0.5,
			opacity: 1,
			thickness: 1,
			bustle:false,
        }, ...data);
    }

	fill() {
        return "hsla(200, 0%, 20%, 1)";
    }
	
	stroke() {
       return "hsla(200, 0%, 10%, 1)";
    }
	
	
	
    get partPrototypes() {
        return [
            {
                side: null,
                Part: CoveredButtPart,
            },
            {
                side: Part.LEFT,
                Part: SuperLegginsPart
            },
            {
                side: Part.RIGHT,
                Part: SuperLegginsPart
            },
        ];
    }
}