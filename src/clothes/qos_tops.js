import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Layer} from "../util/canvas";
import {connectEndPoints, coverNipplesIfHaveNoBreasts} from "../draw/draw";
import { none, extractPoint, drawPoints, splitCurve, breakPoint, clone, adjust} from "drawpoint";
import { straightenCurve, findBetween, lineLineIntersection } from "../util/auxiliary";

export class UndershirtStrapPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "torso",
            reflect            :  true,
            aboveParts: ["parts torso", "decorativeParts torso"],
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        Clothes.simpleStrokeFill(ctx, ex, this);
        //setStrokeAndFill(ctx, { fill: 'green', stroke: 'red' }, ex);
		//check and recalculate cleavage if deep beyond limit
		function checkCleavage(context,bottom){
			if(cleavage.y<bottom.y)cleavage.y=bottom.y+3;
						
			sp = splitCurve(0.5,cleavage,topIn);
			topIn.cp1 = {
				x: sp.left.p2.x+context.curveCleavageX,
				y: sp.left.p2.y+context.curveCleavageY,
			};
		
		}
		
		//CLEAVAGE
		let sp = splitCurve(this.cleavageCoverage,ex.neck.cusp, ex.groin);
		let cleavage = {
			x : -0.1,
			y : sp.left.p2.y
		};
		
		if(this.outerNeckCoverage<this.innerNeckCoverage)this.outerNeckCoverage=this.innerNeckCoverage;
		if(this.innerNeckCoverage>this.outerNeckCoverage)this.innerNeckCoverage=this.outerNeckCoverage;
		
		let cusp = ex.neck.cusp;
		if(ex.trapezius)cusp=ex.trapezius;
		sp = splitCurve(this.outerNeckCoverage,cusp, ex.collarbone);
		let topOut = sp.left.p2;
				
		if(this.innerNeckCoverage<0){
			sp = splitCurve(1+this.innerNeckCoverage, ex.neck.top, cusp);
		}else{
			sp = splitCurve(this.innerNeckCoverage, cusp, ex.collarbone);
		}
		let topIn = extractPoint(sp.left.p2);
				
        let armpit = adjust(ex.armpit,0,0);
        let bottom = {
            x: -0.1,
            y: armpit.y-1
        };		
        
        checkCleavage(this,bottom);
			
        ctx.beginPath();
        drawPoints(ctx,
            cleavage,
            topIn,
            topOut,
            extractPoint(armpit),
            bottom
        );
        ctx.fill();
        
        ctx.beginPath();
        drawPoints(ctx,
            cleavage,
            topIn,
            topOut,
            extractPoint(armpit)
        );	
        ctx.stroke();
        
        return;
		
    }
}

export class UndershirtBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.GENITALS,
            loc       : "chest",
            reflect   : true,
            aboveParts: ["parts chest", "decorativeParts chest"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        Clothes.simpleStrokeFill(ctx, ex, this);

        if (coverNipplesIfHaveNoBreasts(ex, ctx, this)) {
            return;
        }

        const top = adjust(ex.breast.top, 0, 0);
        const tip = adjust(ex.breast.tip, 0.1, 0);
        const bot = adjust(ex.breast.bot, 0, -0.1);
        const inner = adjust(ex.breast.in, 0, 0);
        const cleavage = adjust(ex.breast.cleavage, -0.1, 0);
        const topAgain = adjust(connectEndPoints(cleavage, top), 0, 0.2);


        // top strap
        ctx.beginPath();
        drawPoints(ctx,
            top,
            tip,
            bot,
            inner,
            cleavage,
            topAgain
        );
        ctx.fill();

        // fill out breasts
        ctx.beginPath();
        drawPoints(ctx,
            top,
            tip,
            bot,
            inner
        );
        ctx.stroke();

        }
		
}


export class UndershirtChestPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "torso",
            reflect            : true,
            aboveParts: ["parts torso", "decorativeParts torso"],
        }, {
            
        }, ...data);
    }

    /**
     * Draw an undershirt base. Start in the center of the chest, then to the armpit, down to the hip
     * and through to the pelvis. and back up to the center of the chest. Only stroke the outer lines. 
     * @param {object} ex 
     * @param {HTMLCanvasElement} ctx 
     */
    renderClothingPoints(ex, ctx) {

        Clothes.simpleStrokeFill(ctx, ex, this);
        //setStrokeAndFill(ctx, { fill: 'purple', stroke: 'red' }, ex);

 		//waist points
         const {armpit,lat,waist,hip,out,bottom} = calcTopBody.call(this, ex);
		
         let top = {
            x: -0.1,
            y: ex.armpit.y
        };
         ctx.beginPath();
         drawPoints(ctx, 
             armpit,  
             lat,
             waist,
             hip,
             out,
             bottom,
             top,
             extractPoint(armpit)
         );
         ctx.fill();
         
         ctx.beginPath();
         drawPoints(ctx, 
             armpit, 
             lat,
             waist,
             hip,
             out,
             bottom
         );
         ctx.stroke();
		
    }
}


function calcTopBody(ex){

    let sideLoose = Math.max(0.0, Math.min(this.sideLoose, 1)); // clamp 0 - 1
    let waistCoverage = Math.max(0.0, Math.min(this.waistCoverage, 2)); // clamp 0 - 2
    let thickness = Math.max(0.0, Math.min(this.thickness, 7)); // clamp 0 - 7
	let armpit = clone(ex.armpit);
	let lat = clone(ex.lat);
	//looseness of waist points 
	let hip = adjust(ex.hip, 0, 0);
	let waist = adjust(ex.waist,(thickness * 0.8), 0); //last remnant of sweater
	
	
	//to have top loose around the waist (the same function as for dress)
	{
		let top = armpit;
		if(lat)top = lat;
		let mid = lineLineIntersection(top,hip,{x:0,y:waist.y},{x:100,y:waist.y});
		if(mid.x>waist.x){
			waist.x = findBetween(waist.x, mid.x,sideLoose);
			straightenCurve(armpit,waist,sideLoose);
			straightenCurve(waist,hip,sideLoose);
		};
	}
	
	let out;
	//coverage of waist
	if (waistCoverage > 1){
		let sp = splitCurve(1-(waistCoverage-1),ex.armpit,waist); //THIS????????
		waist = void 0;
		hip = void 0;
		out = sp.left.p2;
	}else if (this.waistCoverage >= 0){
		let sp = splitCurve(1-waistCoverage,waist,hip);
		//waist =  adjust(ex.hip, 0, 0);
		hip = void 0;
		out = sp.left.p2;
    }else{
        let sp = splitCurve(Math.abs(waistCoverage),hip,ex.thigh.out);
		out = sp.left.p2;
    }
	
	//bottom
	let bottom = {
			y:out.y-3,
			x:-0.1,
		};
	
	bottom.cp1 = {
		x: bottom.x * 0.5 + out.x * 0.5,
		y: bottom.y
	};
	
	return {
		armpit:armpit,
		lat:lat,
		waist: waist,
		hip: hip, 
		out:out, 
		bottom: bottom
	};
}

/**
 * Base Clothing classes
 */
export class QueenTops extends Clothing {
    constructor(...data) {
        super(...data);
    };

    stroke() {
        return "hsla(335, 800%, 30%, 1)";
    }

    fill() {
        return "hsla(335, 100%, 42%, 1)";
    }
}

export class QueenUndershirt extends QueenTops {
    constructor(...data) {
        super({
            clothingLayer : Clothes.Layer.INNER,
			cleavageCoverage: 0.3,
			outerNeckCoverage: 0.35,
			innerNeckCoverage: 0.15,
			curveCleavageX: 9,
			curveCleavageY: -9,
			waistCoverage: 0.66,
			sideLoose: 0,
        }, ...data);
    };

    stroke() {
        return none;
    }

    fill() {
        return "ivory";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: UndershirtBreastPart
            },
            {
                side: null,
                Part: UndershirtStrapPart
            },
            { 
                side: null,  
                Part: UndershirtChestPart
            }
        ];
    }
}	


