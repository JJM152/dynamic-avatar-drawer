import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Layer} from "../util/canvas";
import {
    simpleQuadratic,
    drawPoints,
    getPointOnCurve,
    adjust,
    extractPoint,
    tracePoint, none
} from "drawpoint";
import {Location, setStrokeAndFill} from "..";
import {Part} from "../parts/part";

export class MascaraPart extends ClothingPart {
    constructor(...data) {
        super({
                layer     : Layer.BELOW_HAIR,
                loc       : `${Location.EYELASH}`,
                reflect   : false,
                aboveParts: ["faceParts eyelash"]
            }, {
                topFill: "black",
                botFill: "black",
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
        setStrokeAndFill(ctx,
            {
                stroke: none,
                fill  : this.topFill
            },
            ex);

        ctx.beginPath();
        drawPoints(ctx,
            ex.eyes.out,
            ex.eyelid.top,
            ex.eyelid.in,
            ex.eyelash.top,
            ex.eyelash.out);
        ctx.fill();

        setStrokeAndFill(ctx,
            {
                stroke: none,
                fill  : this.botFill
            },
            ex);
        ctx.beginPath();
        drawPoints(ctx,
            ex.eyes.out,
            ex.eyes.in,
            ex.eyelash.outBot);
        ctx.fill();
    }
}

export class Makeup extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.MID,
            requiredParts: "faceParts",
        }, ...data);
    }

}


export class Mascara extends Makeup {
    constructor(...data) {
        super({extraLength: 2}, ...data);
        this.Mods = Object.assign({
            eyelashLength: this.extraLength,
        }, this.Mods);
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: MascaraPart
            },
            {
                side: Part.RIGHT,
                Part: MascaraPart
            },
        ];
    }
}

