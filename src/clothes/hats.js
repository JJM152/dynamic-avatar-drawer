import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";
import {
    simpleQuadratic,
    drawPoints,
    getPointOnCurve,
    adjust,
    extractPoint,
    tracePoint
} from "drawpoint";
import {averagePoint} from "..";

export class CapBasePart extends ClothingPart {
    constructor(...data) {
        super({
                layer: Layer.ABOVE_HAIR,
                loc  : "head",
            },
            {
                // how high the top of the cap should be
                height      : 4.5,
                // [-0.5, 0.5] which side the cap is tilted towards (negative is top tilted towards left)
                sideBias    : -0.15,
                // degree of curve for bottom of hat
                curvature   : 7,
                // [-1, 1] (reasonable values) how much of the forehead should be covered (negative doesn't cover forehead)
                headCoverage: 0.2,
                // how wide to make the hat to adjust for ears and stuff
                sideOffset  : 2.3,
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
        const {rightBot, leftBot, top} = calcCap.call(this, ex);
        Clothes.simpleStrokeFill(ctx, ex, this);

        ctx.beginPath();
        drawPoints(ctx,
            rightBot,
            leftBot,
            top,
            rightBot);
        ctx.fill();
        ctx.stroke();

    }
}

export class CapBandPart extends ClothingPart {
    constructor(...data) {
        super({
                layer     : Layer.ABOVE_HAIR,
                loc       : "+head",
                aboveParts: ["clothingParts head"]
            }, {
                bandWidth  : 1.6,
                bandPattern: "black",
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
        const {rightBot, leftBot, top} = calcCap.call(this, ex);
        const leftTop = adjust(getPointOnCurve(this.bandWidth * 0.1, leftBot, top), -0.5, 0);
        const rightTop = adjust(getPointOnCurve(1-this.bandWidth * 0.1, top, rightBot), 0.5, 0);

        rightTop.cp1 = adjust(leftBot.cp1, 0, rightTop.y - rightBot.y);

        leftTop.cp1 = simpleQuadratic(leftBot, leftTop, 0.5, 1);
        const rightBotAgain = extractPoint(rightBot);
        rightBotAgain.cp1 = simpleQuadratic(rightTop, rightBotAgain, 0.5, 1);

        setStrokeAndFill(ctx,
            {
                fill  : this.bandPattern,
                stroke: this.stroke
            },
            ex);
        ctx.beginPath();
        drawPoints(ctx,
            rightBot,
            leftBot,
            leftTop,
            rightTop,
            rightBotAgain);
        ctx.fill();
        ctx.stroke();

    }
}

export function calcCap(ex) {
    // right to left bottom of hat
    const rightBot = adjust(getPointOnCurve(this.headCoverage, ex.skull.side, ex.skull.bot),
        this.sideOffset,
        0);
    const leftBot = {
        x: -rightBot.x,
        y: rightBot.y
    };
    leftBot.cp1 = simpleQuadratic(rightBot, leftBot, this.sideBias + 0.5, -this.curvature);

    const top = {
        x: this.sideBias * 0.5 * rightBot.x,
        y: ex.skull.y + this.height
    };

    const sideDeflection = 2;
    const sideCpy = averagePoint(leftBot, top, 0.65).y;
    top.cp1 = {
        x: leftBot.x - sideDeflection,
        y: sideCpy
    };

    top.cp2 = {
        x: averagePoint(leftBot, top, 0.5).x,
        y: top.y
    };

    rightBot.cp1 = {
        x: averagePoint(rightBot, top, 0.5).x,
        y: top.y
    };
    rightBot.cp2 = {
        x: rightBot.x + sideDeflection,
        y: sideCpy
    };

    return {
        rightBot,
        leftBot,
        top
    };
}

export class Hat extends Clothing {
    constructor(...data) {
        super({clothingLayer: Clothes.Layer.OUTER}, ...data);
    }

}


export class SimpleCap extends Hat {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: CapBasePart
            }
        ];
    }
}

export class BandedCap extends Hat {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: CapBasePart
            },
            {
                side: null,
                Part: CapBandPart
            },
        ];
    }
}
