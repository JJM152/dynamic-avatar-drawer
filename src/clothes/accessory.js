import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {Layer} from "../util/canvas";
import {
    //simpleQuadratic,
    drawPoints,
    extractPoint,
    splitCurve,
    adjust,
	reflect,
	interpolateCurve,
      breakPoint
} from "drawpoint";
//import {adjustColor} from "../util/utility";

import {
	perpendicularPoint,
	findBetween,
} from "../util/auxiliary";

export class GlassesPart extends ClothingPart {
    constructor(...data) {
        super({
			layer: Layer.BELOW_HAIR,
			loc: "head",
			reflect: true,
			aboveParts: ["eyelid","brow","eyelash"]
		},
		{	
			eccentricity : 5,
			height : 3,
			thickness : 0.5,
        },
		...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		let ear = extractPoint(ex.ear.mid);
		ear.y += 1;
		ear.x -= 1;

		let outerControlPoint = extractPoint(ex.eyes.out);
		outerControlPoint.x += 1;
		
		let toEar = extractPoint(ex.ear.mid);
		toEar.x -= 2;
		toEar.y += 2;

		let innerControlPoint = extractPoint(ex.eyes.in);
		innerControlPoint.x -= 1.8;
		
		const eccentricity = this.eccentricity;
		const height = this.height;
		//let halfControlPoint = {x:findBetween(innerControlPoint.x,outerControlPoint.x),y:findBetween(innerControlPoint.y,outerControlPoint.y) };
		
		let innerControlPointTop =  {x:innerControlPoint.x,y:innerControlPoint.y};
		innerControlPointTop.cp1 = perpendicularPoint(innerControlPoint,outerControlPoint,0.5,height);
		innerControlPointTop.cp1.x += eccentricity;
		innerControlPointTop.cp2 = perpendicularPoint(innerControlPoint,outerControlPoint,0.5,height);
		innerControlPointTop.cp2.x += -eccentricity;
		
		let innerControlPointBot =  {x:innerControlPoint.x,y:innerControlPoint.y};
		innerControlPointBot.cp1 = perpendicularPoint(innerControlPoint,outerControlPoint,0.5,-height);
		innerControlPointBot.cp1.x += eccentricity;
		innerControlPointBot.cp2 = perpendicularPoint(innerControlPoint,outerControlPoint,0.5,-height);
		innerControlPointBot.cp2.x += -eccentricity;
		
		let toNose = reflect(innerControlPoint);

		toNose.cp1 = {
			y:innerControlPoint.y+2,
			x:0
		};			
			
		ctx.beginPath();
		drawPoints(ctx,outerControlPoint,innerControlPointTop,breakPoint, outerControlPoint, innerControlPointBot);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx,toEar,outerControlPoint,innerControlPointTop,breakPoint, outerControlPoint, innerControlPointBot, toNose);
		ctx.stroke();	
	}
}	
		
/*
export class BeltPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "+torso",
			reflect		:true, 
            aboveParts: ["clothingParts leg","clothingParts groin"],
		},
		{	
			waistCoverage: 0.33,
			width:4,
			curve:-5,
			buckle:2,
			highlight:"#cdc331",
        },
		...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		let temp;
		
		//TOP
		let topOut;
		if(this.waistCoverage>0){
			let temp = splitCurve((1-this.waistCoverage),ex.waist,ex.hip);	
			topOut = extractPoint(temp.left.p2);
		}else{
			let temp = splitCurve(Math.abs(this.waistCoverage),ex.hip,ex.thigh.out);	
			topOut = extractPoint(temp.left.p2);
		}
	
		//BOT
		let botOut;
		if(topOut.y-this.width>ex.hip.y){
			botOut = interpolateCurve(ex.waist, ex.hip,{
				x: null,
				y: topOut.y-this.width
			});
			botOut=botOut[0];
		}else{
			botOut = interpolateCurve(ex.hip, ex.thigh.out,{
				x: null,
				y: topOut.y-this.width
			});
			botOut=botOut[0];
		}
		
		let topIn = {
			x: -0.1,  
			y: topOut.y + this.curve
		};
		
		let botIn = {
			x: -0.1,  
			y:	topIn.y - this.width
		};

		
		//BUCKLE
			temp = splitCurve(this.buckle/10,topIn,topOut);	
		let topMid = extractPoint(temp.left.p2);
		topIn = {
			x:topIn.x,
			y:topMid.y
		};
	
	 
			temp = splitCurve(this.buckle/10,botIn,botOut);	
		let botMid = {
			x:topMid.x,
			y:temp.left.p2.y
		};
		botIn = {
			y:botMid.y,
			x:topIn.x,
		};
		
		ctx.beginPath();
        drawPoints(ctx, 
			topIn,
			topMid,
			topOut,
			botOut,
			botMid,
			botIn
		);
        ctx.fill();
        ctx.stroke();
		
		ctx.fillStyle=this.highlight;
		const adjustement=1;
		ctx.beginPath();
        drawPoints(ctx, 
			adjust(topIn,-0.2,adjustement),
			adjust(topMid,0,adjustement),
			adjust(botMid,0,-adjustement),
			adjust(botIn,-0.2,-adjustement)
		);
        ctx.fill();
        ctx.stroke();
	}
}
*/

/**
 * Base Clothing classes
 */
export class Accessory extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.MID,
        }, ...data);
    }
}


/**
 * Concrete Clothing classes
 */
export class Glasses extends Accessory {
    constructor(...data) {
        super(...data);
    }

	fill() {
        return "#a2a2a2";
    }
	
	stroke() {
       return "#1e1e1e";
    }
	
    get partPrototypes() {
        return [
            {
                side: null,
                Part: GlassesPart,
            },
        ];
    }
}

/*
export class Belt extends Accessory {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.MID,
        },...data);
    }

	fill() {
        return "#464646";
    }
	
	stroke() {
       return "#1e1e1e";
    }
	
    get partPrototypes() {
        return [
            {
                side: null,
                Part: BeltPart,
            },
        ];
    }
}
*/