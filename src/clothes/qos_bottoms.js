import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Layer} from "../util/canvas";
import {
    extractPoint,
	drawPoints, 
	splitCurve,
	breakPoint,
	clone,
	none,
	adjust,
	reflect,
	interpolateCurve,
} from "drawpoint";
import { getLimbPoints, getLimbPointsNegative, getLimbPointsBellowPoint, getLimbPointsAbovePoint, findBetween} from "../util/auxiliary";

export class SkirtBasePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc       : "+torso",
            aboveParts: ["parts leg", "parts groin"],
            belowSameLayerParts: ["torso"],
            reflect            : true
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        let out;
        let hip;
        let thighOut;

        // clamp args
        let waistCoverage = Math.max(0, Math.min(this.waistCoverage, 1));
        let legCoverage = Math.max(0, Math.min(this.legCoverage, 1));
        let legLoose = Math.max(0, Math.min(this.legLoose, 2));

        //TOP
        if(waistCoverage > 0){
            let temp = splitCurve((1-waistCoverage),ex.waist,ex.hip);	
            out = extractPoint(temp.left.p2);
            hip = temp.right.p2;
            thighOut = ex.thigh.out;
        }else{
            let temp = splitCurve(Math.abs(waistCoverage),ex.hip,ex.thigh.out);	
            out = extractPoint(temp.left.p2);
            hip = extractPoint(ex.hip);
            thighOut = temp.right.p2;
        }
    
        const waistCurve = out.y - ex.hip.y;
        let top = {
            x: -0.1,  
            y: ex.pelvis.y + waistCurve * 1.2
        };

        out.cp1 = {
            x: out.x * 0.5 + top.x * 0.5,
            y: top.y
        };

	    const addPointsMid = []; //ie should be muscles included?
		if(typeof ex.quads !== "undefined"){
            addPointsMid[0] =  clone(ex.quads.top);
            addPointsMid[0].x = thighOut.x;
            addPointsMid[1] = clone(ex.quads.out);
            addPointsMid[2] = clone(ex.knee.out);
        }
        
        let legPoints = [ ];

		if(legLoose > 0){
			legPoints = getLimbPoints(hip, ex.ankle.out, legCoverage, hip, thighOut, ...addPointsMid);
			let totalLegLength = hip.y-( (hip.y-ex.ankle.out.y) * legCoverage );
			if (totalLegLength < ex.groin.y){ 
				legPoints = [];
				legPoints[legPoints.length] = clone(hip);
				legPoints[legPoints.length] = clone(ex.thigh.out);
				
				legPoints[legPoints.length-1].y = totalLegLength;
				legPoints[legPoints.length-1].x += legLoose * 30 * legCoverage;
				
			} 
		}else{
            legPoints = getLimbPoints(hip, ex.ankle.out, legCoverage,
                    hip, thighOut, ...addPointsMid, ex.knee.out, ex.calf.out, ex.ankle.out);
        }
        
        let innerBottom = {
            x: -0.1,
            y: legPoints[legPoints.length-1].y
        } 


        Clothes.simpleStrokeFill(ctx, ex, this);

        ctx.beginPath();
        drawPoints(ctx,
            top, 
            out,
            hip,
            ...legPoints,
            innerBottom,
            top
        );

        ctx.fill();

        ctx.beginPath();
        drawPoints(ctx, 
            top, 
            out,
            hip,
            ...legPoints,
            innerBottom
        );
        
        ctx.stroke();
    }

}
/**
 * Base Clothing classes
 */
export class QueenBottoms extends Clothing {
    constructor(...data) {
        super(...data);
    };

    stroke() {
        return "hsla(335, 800%, 30%, 1)";
    }

    fill() {
        return "hsla(335, 100%, 42%, 1)";
    }
}

export class QueenSkirt extends QueenBottoms {
    constructor(...data) {
        super({
            clothingLayer  : Clothes.Layer.MID,
            waistCoverage: 0.3,
			legCoverage: 0.4,
			legLoose: 0,
        }, ...data);
    };

    stroke() {
        return none;
    }

    fill() {
        return "lightpink";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: SkirtBasePart
            },

        ];
    }
}