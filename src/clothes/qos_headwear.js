import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";
import {
    none,
    point,
    breakPoint,
    simpleQuadratic,
    drawPoints,
    drawCircle,
    getPointOnCurve,
    makePoint,
    adjust,
    extractPoint,
    tracePoint,
    scalePoints,
    rotatePoints
} from "drawpoint";
import {averagePoint} from "..";
import { straightenCurve, findBetween } from "../util/auxiliary";


export class QueenHat extends Clothing {
    constructor(...data) {
        super(
            {clothingLayer: Clothes.Layer.OUTER
            }, ...data);
    }
}

// Nun habit.
export class QueenHabitBase extends ClothingPart {
    constructor(...data) {
        super({
                layer     : Layer.HAIR,
                loc       : "head",
                reflect   : true,
                aboveParts: ["parts head"]
            },
            ...data);
    }

    renderClothingPoints(ex, ctx)
    {

        let outerPoints = [ ex.skull, ex.skull.side, ex.ear.mid, ex.skull.bot, ex.chin.out, ex.chin.bot ];
        let innerPoints = [ 
            adjust(extractPoint(ex.lips.bot), 0, -2), 
            adjust(extractPoint(ex.lips.out), findBetween(ex.lips.out.x, ex.lips.bot.x, 0.5), -2), 
            adjust(ex.neck.top, 1, 0), 
            adjust(ex.brow.outtop, 1, 1) ];
        innerPoints[3].cp1.x = innerPoints[3].x + 4;
        innerPoints[4] = { x: innerPoints[0].x, y: innerPoints[3].y };

        setStrokeAndFill(ctx, 
            {
                fill: this.ScapularFill,
                stroke: this.ScapularStroke,
            }, ex);

        //Draw head covering
        ctx.beginPath();
        drawPoints(ctx, 
            ...outerPoints,
            ...innerPoints,
            outerPoints[0]
            );
        ctx.fill();

        ctx.beginPath();
        drawPoints(ctx,
            ...outerPoints
            );
        ctx.lineWidth = 0.5;
        ctx.stroke();
        
    }
}

export class QueenHabitTop extends ClothingPart {
    constructor(...data) {
        super({
                layer     : Layer.ABOVE_HAIR,
                loc       : "head",
                reflect   : true,
                aboveParts: ["parts head"]
            },
            ...data);
    }

    renderClothingPoints(ex, ctx)
    {

        let outerPoints = [ 
            adjust(extractPoint(ex.skull), 0, 4), 
            adjust(extractPoint(ex.skull.side), 4, 0) ];
        outerPoints[1].cp1 = simpleQuadratic(outerPoints[0], outerPoints[1], 0.3, 5);
        outerPoints[1].cp2 = simpleQuadratic(outerPoints[0], outerPoints[1], 0.6, 7);
        let innerPoints = [];
        innerPoints[0] = { x: ex.skull.x, y: ex.skull.side.y };

        Clothes.simpleStrokeFill(ctx, ex, this);

        //Draw head covering
        ctx.beginPath();
        drawPoints(ctx,
            ...outerPoints,
            ...innerPoints,
            outerPoints[0]
            );
        ctx.fill();

        ctx.beginPath();
        drawPoints(ctx,
            ...outerPoints
            );
        ctx.lineWidth = 0.5;
        ctx.stroke();
        
    }
}

export class QueenHabitBand extends ClothingPart {
    constructor(...data) {
        super({
                layer     : Layer.ABOVE_HAIR,
                loc       : "+head",
                reflect   : true,
                aboveParts: ["clothingParts head"]
            }, 
            ...data);
    }

    renderClothingPoints(ex, ctx) {
 
        let outerPoints = [
            { x: ex.skull.x, y: findBetween(ex.skull.y, ex.skull.side.y, 0.5) },
            { x: ex.skull.side.x +4 , y: findBetween(ex.skull.y, ex.skull.side.y, 0.5) },
            adjust(extractPoint(ex.skull.side), 4, 0),
            { x: ex.skull.x, y: ex.skull.side.y }
        ];

        setStrokeAndFill(ctx,
            {
                fill  : this.ScapularFill,
                stroke: this.ScapularStroke
            },
            ex);

        ctx.beginPath();
        drawPoints(ctx,
            ...outerPoints,
            outerPoints[0]);

        ctx.fill();

        ctx.beginPath();
        drawPoints(ctx,
            ...outerPoints);
        ctx.stroke();

        }
}

export class QueenHabitBack extends ClothingPart {
    constructor(...data) {
        super({
                layer     : Layer.BACK,
                loc       : "head",
                reflect   : true,
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
 
        let outerPoints = [ 
            adjust(extractPoint(ex.skull), 0, 4), 
            adjust(extractPoint(ex.skull.side), 4, 0),
            adjust(extractPoint(ex.collarbone), 4, 0),
            { x: 0, y: ex.collarbone.y }
         ];

         Clothes.simpleStrokeFill(ctx, ex, this);

        ctx.beginPath();
        drawPoints(ctx,
            ...outerPoints,
            outerPoints[0]
            );
        ctx.fill();

        ctx.beginPath();
        drawPoints(ctx,
            ...outerPoints
            );
        ctx.lineWidth = 0.5;
        ctx.stroke();

        }
}

export class QueenHabit extends QueenHat {
    constructor(...data) {
        super( {
            clothingLayer: Clothes.Layer.OUTER,
            ScapularFill: "ivory",
            ScapularStroke: "dimgrey",
            Mods: {
                    hairStyle: -100, // kludge!
                    hairLength: -100
            },
        }, ...data);
    }

    fill(){
        return "black";
    }

    stroke() {
        return none;
    }

    get partPrototypes()
    {
        return [
            { 
                side: null,
                Part: QueenHabitBase
            },
            {
                side: null,
                Part: QueenHabitTop
            },
            {
                side: null,
                Part: QueenHabitBand
            },
            {
                side: null,
                Part: QueenHabitBack
            }
        ]
    }
}

//Hair Pin.

export class QueenHairPinBase extends ClothingPart {
    constructor(...data) {
        super({
                layer     : Layer.BACK,
                loc       : "head",
                reflect   : false,
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {



        let center = { x: 0 + this.xOffset, y: ex.skull.y + this.yOffset };

        let pinPoints = [
            adjust(center, this.pinWidth, this.pinLength), // top right
            adjust(center, -this.pinWidth, this.pinLength), // top left
            { x: center.x, y: -this.pinLength },
        ];

        pinPoints[2].cp1 = simpleQuadratic(pinPoints[0], pinPoints[2], 0.8, 1);
        pinPoints[2].cp2 = simpleQuadratic(pinPoints[1], pinPoints[2], 0.8, 1);

        scalePoints(center, this.hairpinSize, ...pinPoints);
        rotatePoints(center, this.rotateAngle, ...pinPoints);

        Clothes.simpleStrokeFill(ctx, ex, this);

        ctx.save();
        ctx.beginPath();
        drawPoints(ctx,
            ...pinPoints,
            pinPoints[0]);
        ctx.fill();
        ctx.stroke();
        ctx.restore();

        // Draw circle.
        let circCent = { x: 0 + this.xOffset, y: center.y  };
        let circlePoints = drawCircle(circCent, this.ballSize);
        setStrokeAndFill(ctx, this, {
            fill: this.ballFill,
            stroke: this.ballStroke
        });

        ctx.beginPath();
        drawPoints(ctx,
            breakPoint,
            ...circlePoints);
        ctx.fill();
        ctx.stroke();
    }
}

export class QueenHairPin extends QueenHat {
    constructor(...data) {
        super( {
            clothingLayer: Clothes.Layer.OUTER,
            ballFill: "gold",
            ballStroke: "goldenrod",
            hairpinSize: 0.15,
            ballSize: 1.6,
            hairpinSize: 0.13,
            pinLength: 11,
            pinWidth: 4.7,
            rotateAngle: 1.25,
            xOffset: -22,
            yOffset: -15,
        }, ...data);
    }

    fill(){
        return "gold";
    }

    stroke() {
        return "goldenrod";
    }

    get partPrototypes()
    {
        return [
            { 
                side: null,
                Part: QueenHairPinBase
            },
        ]
    }
}

// Pirate Hat

export class QueenPirateHatBack extends ClothingPart {
    constructor(...data) {
        super({
                layer     : Layer.ABOVE_HAIR,
                loc       : "head",
                reflect   : true,
                belowSameLayerParts: ["clothingParts head"]
            },
            ...data);
    }

    renderClothingPoints(ex, ctx)
    {

        //Head covering
        let h1 = adjust(extractPoint(ex.skull), 0, 7);
        let h2 = adjust(extractPoint(ex.skull.side), 0, 2);
        h2.cp1 = simpleQuadratic(h1, h2, 0.23, 6);
        h2.cp2 = simpleQuadratic(h1, h2, 0.66, 5);
        let h3 = { 
            x: h1.x, 
            y: h2.y + 2
        };

        let points = [
            h1, h2, h3
        ];

        Clothes.simpleStrokeFill(ctx, ex, this);

        ctx.beginPath();
        drawPoints(ctx,
            ...points,
            points[0]);

        ctx.fill();

        ctx.beginPath();
        drawPoints(ctx,
            points[0], 
            points[1]);
        ctx.lineWidth = 0.5;
        ctx.stroke();

        
    }
}

export class QueenPirateHatFront extends ClothingPart {
    constructor(...data) {
        super({
                layer     : Layer.ABOVE_HAIR,
                loc       : "head",
                reflect   : false,
                aboveParts: ["clothingParts head"],
                aboveSameLayerParts: ["clothingParts head"]
            },
            ...data);
    }

    renderClothingPoints(ex, ctx)
    {
        let oX = Math.max( (ex.ear.mid.x - 15), 0);
        console.log(oX);
        let h1 = adjust(extractPoint(ex.ear.mid), 2, 0);

        let h2 = point(h1.x + 20, h1.y + 12);
        h2.cp1 = simpleQuadratic(h1, h2, 0.33, -1);
        h2.cp2 = simpleQuadratic(h1, h2, 0.66, -1);

        let h3 = adjust(extractPoint(ex.eyes.top), 0, 25);
        h3.cp1 = simpleQuadratic(h2, h3, 0.33, 2.5);
        h3.cp2 = simpleQuadratic(h2, h3, 0.66, 2);

        let h4 = adjust(extractPoint(h3), -16, 0);
        h4.x -= oX;

        h4.cp1 = simpleQuadratic(h3, h4, 0.20, 22);
        h4.cp2 = { x: h4.cp1.x -18, y: h4.cp1.y+5 };

        let h5 = point( h4.x - 25, h2.y + 3);
        h5.x -= oX;

        h5.cp1 = simpleQuadratic(h4, h5, 0.33, 1);
        h5.cp2 = simpleQuadratic(h4, h5, 0.66, 1);

        let h6 = adjust(extractPoint(h1), -34, 1);
        h6.x -= ( oX * 2.0);

        h6.cp1 = simpleQuadratic(h5, h6, 0.33, -1);
        h6.cp2 = simpleQuadratic(h5, h6, 0.66, -1);

        let h7 = adjust(extractPoint(h6), 1, 2);

        let h8 = adjust(extractPoint(h1), -1, 3);

        h8.cp1 = simpleQuadratic(h7, h8, 0.25, 7);
        h8.cp2 = simpleQuadratic(h7, h8, 0.75, 7);

        let points = [
            h1, h2, h3, h4, h5, h6,
            h7, h8
        ];

        let strokePoints = 
        [
            h1, h2, h3, h4, h5, h6, 
            breakPoint,
            h7, h8
        ];

        Clothes.simpleStrokeFill(ctx, ex, this);

        ctx.beginPath();
        drawPoints(ctx,
            ...points,
            points[0]);
        ctx.fill();

        ctx.beginPath();
        drawPoints(ctx, 
            ...strokePoints);
        ctx.lineWidth = 0.5;
        ctx.stroke();
        

        //Draw skull
        let s1 = { x: h1.x, y: h2.y +6 };
        let s2 = { x: s1.x - 4, y: s1.y -1.5 };
        s2.cp1 = simpleQuadratic(s1, s2, 0.33, -1.75);
        s2.cp2 = simpleQuadratic(s1, s2, 0.70, -1.5);

        let s3 = { x: s1.x -2, y: s2.y -3.5 };
        s3.cp1 = simpleQuadratic(s2, s3, 0.33, -1.5);
        s3.cp2 = simpleQuadratic(s2, s3, 0.70, -1.5);

        let s4 = { x: s3.x +0.5, y: s3.y -2.5 };
        s4.cp1 = simpleQuadratic(s3, s4, 0.70, -1.24);
        s4.cp2 = simpleQuadratic(s3, s4, 0.80, -1.24);

        let s5 = { x: s4.x +2.2, y: s4.y + 1.7 };
        s5.cp1 = simpleQuadratic(s4, s5, 0.20, -1.24);
        s5.cp2 = simpleQuadratic(s4, s5, 0.30, -1.24);

        let s6 = { x: s5.x +3, y: s5.y + 2.15 };
        s6.cp1 = simpleQuadratic(s5, s6, 0.33, -1.24);
        s6.cp2 = simpleQuadratic(s5, s6, 0.70, -1.24);

        s1.cp1 = simpleQuadratic(s6, s1, 0.33, -1.5);
        s1.cp2 = simpleQuadratic(s6, s1, 0.70, -1.25);

        let e1 = { x: s1.x -1.5, y: s1.y - 1 };
        let e2 = { x: e1.x -1, y: e1.y - 2.5 };
        e2.cp1 = simpleQuadratic(e1, e2, 0.23, -1);
        e2.cp2 = simpleQuadratic(e1, e2, 0.70, -1);
        e1.cp1 = simpleQuadratic(e2, e1, 0.23, -2);
        e1.cp2 = simpleQuadratic(e2, e1, 0.70, -2);

        let e3 = { x: s1.x+ 1.9, y: s1.y - 1.75 };
        let e4 = { x: e3.x -1, y: e3.y - 2.5 };
        e4.cp1 = simpleQuadratic(e3, e4, 0.23, -1);
        e4.cp2 = simpleQuadratic(e3, e4, 0.70, -1);
        e3.cp1 = simpleQuadratic(e4, e3, 0.23, -2);
        e3.cp2 = simpleQuadratic(e4, e3, 0.70, -2);

        let eyeHoles = [
            breakPoint,
            e1, e2, e1,
            breakPoint,
            e3, e4, e3
        ];

        let skullPoints = 
        [
            s1, s2, s3, s4, s5, s6, s1
        ];

        setStrokeAndFill(ctx, 
            {
                fill: this.SkullFill,
                stroke: "black",
            }, ex);

        ctx.beginPath()
        drawPoints(ctx, 
            ...skullPoints);
        ctx.lineWidth = 0.25;
        ctx.fill();

        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath()
        drawPoints(ctx, 
            ...eyeHoles);
        ctx.lineWidth = 0.25;
        ctx.fill();
        }
}


export class QueenPirateHat extends QueenHat {
    constructor(...data) {
        super( {
            clothingLayer: Clothes.Layer.OUTER,
            SkullFill: "white",
        }, ...data);
    }

    fill(){
        return "#444444";
    }

    stroke() {
        return "#ffcc00";
    }

    get partPrototypes()
    {
        return [
            {
                side: null,
                Part: QueenPirateHatBack
            },
            { 
                side: null,
                Part: QueenPirateHatFront
            },
        ]
    }
}