import {shadingMedium, ShadingPart} from "../draw/shading_part";
import {Layer} from "../util/canvas";
import {BodyPart} from "./part";
import {
    adjust,
    splitCurve,
    clamp,
    simpleQuadratic,
    fillerDefinition,
    endPoint,
} from "drawpoint";

function calcTorsoShading(ex) {
    let armpitBase = ex.armpit;
    if (ex.breast) {
        const sp = splitCurve(0.1, ex.armpit, ex.waist);
        armpitBase = sp.left.p2;
    }
    const armpit = adjust(armpitBase, -2 - this.shoulderWidth * 0.03, -5);
    const waist = adjust(ex.waist, -1 - this.waistWidth * 0.03, -4);
    const hip = adjust(ex.hip, -3 - this.hipWidth * 0.04, 0);
    hip.cp1 = adjust(hip.cp1, this.hipWidth * 0.02, 0);

    const outAmount = 5;
    const outBot = {
        x: ex.hip.x + outAmount,
        y: ex.hip.y
    };
    const outMid = {
        x: ex.waist.x + outAmount,
        y: ex.waist.y
    };
    const outTop = {
        x: ex.armpit.x + outAmount,
        y: armpitBase.y
    };
    armpit.cp1 = simpleQuadratic(outTop, armpit, 0.7, -4);
    return [armpit, waist, hip, outBot, outMid, outTop, armpit];
}

class TorsoShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "+torso",
            layer: Layer.FRONT,
        }, ...data);
    }

    fill() {
        return shadingMedium;
    }

    calcDrawPoints(ex) {
        return calcTorsoShading.call(this, ex);
    }
}


class Torso extends BodyPart {
    constructor(...data) {
        super({
            loc         : "torso",
            forcedSide  : null,
            reflect     : true,
            layer       : Layer.FRONT,
            shadingParts: [TorsoShading],
        }, ...data);
    }
}


export class TorsoHuman extends Torso {
    constructor(...data) {
        super(...data);
    }

    calcDrawPoints(ex, mods, calculate) {

        if (calculate) {
            // remaining height...
            fillerDefinition(ex, "armpit", {
                x: ex.collarbone.x - this.upperMuscle * 0.03,
                y: ex.collarbone.y - 12 - this.upperMuscle * 0.1
            });

            if (ex.hasOwnProperty("elbow")) {
                let sp = splitCurve(clamp(1.2 - this.upperMuscle / 40, 0.6, 1), ex.elbow.in,
                    ex.armpit);
                ex.lat = sp.right.p1;
                ex.lat.cp1 = sp.right.p2.cp2;
                ex.lat.cp2 = sp.right.p2.cp1;
            } else {
                fillerDefinition(ex,
                    "lat",
                    {
                        x: ex.armpit.x + 0.5,
                        y: ex.armpit.y - 3
                    });
            }

            const shoulderCausedTorsoWidth = (this.shoulderWidth - 80) * 0.1;
            ex.waist = {
                x  : this.waistWidth * 0.1 + shoulderCausedTorsoWidth * 0.7,
                y  : ex.armpit.y - this.torsoLength * 0.33,
                cp1: {
                    x: ex.armpit.x,
                    y: ex.armpit.y - 5
                },
            };
            ex.waist.cp2 = {
                x: ex.waist.x,
                y: ex.waist.y + this.waistWidth * 0.01 + shoulderCausedTorsoWidth * 0.1
            };

            ex.hip = {
                x  : this.hipWidth * 0.1 + shoulderCausedTorsoWidth * 0.5,
                y  : ex.waist.y - this.torsoLength * 0.32,
                cp1: {
                    x: ex.waist.x,
                    y: ex.waist.y - this.torsoLength * 0.2
                },
            };
            ex.hip.cp2 = {
                x: ex.hip.x - this.buttFullness * 0.03 - this.legFem * 0.05,
                y: ex.hip.y + 4 + this.buttFullness * 0.015,
            };

            ex.pelvis = {
                x: 0,
                y: ex.hip.y - this.torsoLength * 0.05
            };
        }

        // normal, can be closed by leg
        if (ex.hasOwnProperty("knee")) {
            return [ex.armpit, ex.lat, ex.waist, ex.hip, endPoint];
        } else {
            return [ex.armpit, ex.lat, ex.waist, ex.hip, ex.groin];
        }
    }
}
