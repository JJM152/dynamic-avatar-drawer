import {DecorativePart} from "./decorative_part";
import {Layer} from "../util/canvas";
import {none, extractPoint} from "drawpoint";
import {findBetween} from "../util/auxiliary";

export class QoSBeard extends DecorativePart {
    constructor(...data) {
        super({
            loc       : "head",
            reflect   : true,
            layer     : Layer.BELOW_HAIR,
            aboveParts: ["parts head"],
            beardLength: 0.5,
            beardWidth: 0.5
        }, ...data);
    }
    
    stroke() {
        return none;
    }

    fill(ignore, ex) {
         return ex.hairFill;
    }

    clipFill() {
        
    }
    
    calcDrawPoints(ex, ignore, calculate, part) {


        if (calculate) {
        
        ex.beard = { };
        ex.beard.top = extractPoint(ex.lips.bot);
        ex.beard.top.y -= 2;

        ex.beard.in = extractPoint(ex.lips.out);
        ex.beard.in.x += 2;

        ex.beard.bot = extractPoint(ex.chin.bot);
        ex.beard.bot.y = findBetween(ex.chin.bot.y, ex.armpit.y, (1 - part.beardLength));

        ex.beard.out = extractPoint(ex.chin.out);
        ex.beard.out.x = findBetween(ex.chin.out.x, ex.beard.bot.x, part.beardWidth);
        ex.beard.out.y = ex.beard.bot.y+1;
        ex.beard.out.cp1 = {
                x: ex.beard.out.x + 4,
                y: ex.beard.bot.y + 4,
            }
        } 

        return [
            ex.skull.bot, ex.jaw,
            ex.chin.out, 
            ex.beard.out,
            ex.beard.bot,
            ex.beard.top, 
            ex.beard.in, 
            ex.neck.top, ex.skull.bot
        ];

    }
}
