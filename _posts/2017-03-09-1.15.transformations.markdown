---
layout: post
title:  "1.15 Interpolation and Transformation"
date:   2017-03-09
categories: release
load_library: true
---

- new drawing mechanisms: curve interpolation and curve transformation.
- new clothing template: dress shirts

## Dress Shirt Template

![Chained transformations and dress shirt](http://i.imgur.com/PIRYX0p.gif)

The new dress shirt template comes with many drawing parts and parameters.
Also demonstrated in the GIF is the new ability to chain transformations.
Previously transformations were asynchronous processes that returned immediately,
but now they return promises that can be chained together via .then(), such as shown below:

{% highlight javascript %}
function drawAndExport() {
    da.draw(window.canvasGroup, PC, window.config).then(function (exports) {
        window.ex = exports;
    });
}

var transformation = da.createTransformation(myClothing, drawAndExport, {
    topParted: 8,
    botParted: 8
});
var breastEnlarge = da.createTransformation(PC, drawAndExport, {
    basedim: {
        breastSize: 20
    }
});
var breastShrink = da.createTransformation(PC, drawAndExport, {
    basedim: {
        breastSize: -20
    }
});

// sequential transformations
da.transformAndShow(transformation, 3000)
.then(da.transformAndShow.bind(null, breastEnlarge, 2500))
.then(da.transformAndShow.bind(null, breastShrink, 2500));
{% endhighlight %}

Note that transformations can apply to any object, as shown by the first transformation
which is on the clothing rather than the PC.

## Curve Interpolation

The new method `interpolateCurve` gives back a point on a curve given partial information
about the point (either its x or y). The demo below shows this in action.
**Click and hold somewhere on the canvas below**.

<canvas markdown="0" id="interpolate" height="600" width="600"></canvas>
<script markdown="0">
    function circle(ctx, point) {
        ctx.beginPath();
        ctx.arc(point.x,point.y,3,0,2*Math.PI);
        ctx.stroke();
    }

    (function(){

    var y;
    var canvas = document.getElementById("interpolate");
    var ctx = canvas.getContext("2d");

    canvas.addEventListener("mousemove",(event)=>{
        y = event.clientY - canvas.getBoundingClientRect().top;
    });

    var mouseDown = false;
    canvas.addEventListener("mousedown",()=>{
        mouseDown = true;
        window.requestAnimationFrame(drawIntersect);
    });
    canvas.addEventListener("mouseup",()=>{
        mouseDown = false;
    });

    function drawIntersect() {
        ctx.clearRect(0,0,canvas.width, canvas.height);

        ctx.lineWidth = 2;
        const p0 = {x:3,y:24};
        const p3 = {x:500,y:560};
        p3.cp1 = {x:20,y:700};
        p3.cp2 = {x:450,y:20};
        ctx.beginPath();
        da.drawPoints(ctx, p0,p3);
        ctx.stroke();

        ctx.strokeStyle = "red";
        ctx.beginPath();
        da.drawPoints(ctx,{x:0,y:y},{x:canvas.width,y:y});
        ctx.stroke();

        var point = {x:null, y:y};
        var ts = da.interpolateCubic(p0.y, p3.cp1.y, p3.cp2.y, p3.y, y);
        ts.forEach((t)=>{
         var x = da.getCubicValue(t,p0.x,p3.cp1.x,p3.cp2.x,p3.x);
            ctx.beginPath();
         ctx.arc(x,y,3,0,Math.PI*2);
         ctx.stroke();
        });
        ctx.strokeStyle = "black";

        if (mouseDown) {
            window.requestAnimationFrame(drawIntersect);
        }
    }
    drawIntersect(0);
    }());

</script>


## Curve Transformation


The new method `transformCurve` gives back an intermediate curve when given a starting
and ending curve.
**Click and hold somewhere on the canvas below**.

<canvas id="transform" height="600" width="600"></canvas>
<script markdown="0">
    function circle(ctx, point) {
        ctx.beginPath();
        ctx.arc(point.x,point.y,3,0,2*Math.PI);
        ctx.stroke();
    }

    (function(){

    var y;
    var canvas = document.getElementById("transform");
    var ctx = canvas.getContext("2d");

    canvas.addEventListener("mousemove",(event)=>{
        y = event.clientY - canvas.getBoundingClientRect().top;
    });

    var mouseDown = false;
    canvas.addEventListener("mousedown",()=>{
        mouseDown = true;
        window.requestAnimationFrame(drawIntersect);
    });
    canvas.addEventListener("mouseup",()=>{
        mouseDown = false;
    });

    function drawIntersect() {
        ctx.clearRect(0,0,canvas.width, canvas.height);

        ctx.lineWidth = 2;
        const p0 = {x:3,y:24};
        const p3 = {x:500,y:560};
        p3.cp1 = {x:20,y:300};
        p3.cp2 = {x:450,y:20};
        ctx.strokeStyle = "red";
        ctx.beginPath();
        da.drawPoints(ctx, p0,p3);
        ctx.stroke();
        circle(ctx,p3.cp1);
        circle(ctx,p3.cp2);
        ctx.beginPath();
        ctx.moveTo(p0.x,p0.y);
        ctx.lineTo(p3.cp1.x, p3.cp1.y);
        ctx.moveTo(p3.x,p3.y);
        ctx.lineTo(p3.cp2.x, p3.cp2.y);
        ctx.stroke();
        ctx.strokeStyle = "blue";

        // transform into curve p0 p3
        // pp0 = {x:25,y:20};
        pp0 = p0;
        pp3 = {x:540,y:300};
        pp3.cp1 = {x:350,y:50};
        ctx.beginPath();
        da.drawPoints(ctx,pp0,pp3);
        ctx.stroke();
        circle(ctx,pp3.cp1);
        ctx.beginPath();
        ctx.moveTo(pp0.x,pp0.y);
        ctx.lineTo(pp3.cp1.x, pp3.cp1.y);
        ctx.lineTo(pp3.x,pp3.y);
        ctx.stroke();

        if (y) {
            ppp3 = da.transformCurve(pp0,pp3,p0,p3, y/canvas.height);
            ppp0 = p0;
            ctx.strokeStyle = "purple";
            ctx.beginPath();
            da.drawPoints(ctx,p0,ppp3);
            ctx.stroke();
            circle(ctx,ppp3.cp1);
            circle(ctx,ppp3.cp2);
            ctx.beginPath();
            ctx.moveTo(ppp0.x,ppp0.y);
            ctx.lineTo(ppp3.cp1.x, ppp3.cp1.y);
            ctx.moveTo(ppp3.x,ppp3.y);
            ctx.lineTo(ppp3.cp2.x, ppp3.cp2.y);
            ctx.stroke();
            ctx.strokeStyle = "black";
        }

        if (mouseDown) {
            window.requestAnimationFrame(drawIntersect);
        }
    }
    drawIntersect(0);
    }());

</script>
