const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    context     : path.resolve(__dirname, './src'),
    entry       : {
        da: "./index.js",
    },
    output      : {
        filename     : '[name]_twine.js',
        path         : path.resolve(__dirname, 'dist'),
        publicPath   : '/',
        library      : 'da',
        libraryTarget: 'umd',
    },
    devServer   : {
        contentBase: './dist',
        openPage   : 'test.html'
    },
    optimization: {
        minimizer: [
            new UglifyJSPlugin({
                cache        : true,
                parallel     : true,
                uglifyOptions: {
                    compress: true,
                    ecma    : 6,
                    keep_fnames: true,
                    keep_classnames: true,
                },
                sourceMap    : true
            })

        ]
    },
    devtool     : 'eval-cheap-module-source-map',
    module      : {
        rules: [
            {
                test   : /\.js$/,
                include: [/src/],
                use    : {
                    loader : 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
        ]
    },
};
